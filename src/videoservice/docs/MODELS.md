Model Architecture Planning

Membership 
	-slug
	-type (free, pro, enterprise)
	-orice
	-stripe plan id

UserMembership
	-user						(foreignkey to default user)
	-stripe customer id
	-membership type  			(foreignkey to Membership)

Subscription
	-user membership			(foreignkey to UserMembership)
	-stripe subscription id 	
	-active

Course
	-slug
	-title
	-description
	-allowed memberships 		(manytomany to Membership)

Lesson
	-slug
	-title
	-course 					(foreignkey to Course)
	-position
	-video
	-thumbnail